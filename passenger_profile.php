<?php
session_start();
if(!isset($_SESSION["id"]) || !isset($_SESSION["username"])){
  
    header("location: login.php?error=notloggedin");
    exit();
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="Profile.css">
    <title>Document</title>
</head>

<body>
    <?php
        
        include 'passenger_nav.php';
        require_once "includes/dbh.inc.php";
        $id = $_SESSION["id"];
        $sql = "SELECT * FROM `passenger` Where `id`= $id; ";  
        $query = mysqli_query($conn, $sql); 
        $row = mysqli_num_rows($query);
        
        $result = mysqli_fetch_assoc($query);
    ?>
    <section class="main">
       
        <div class="profile">
            <div class="img">
                <img src="passengers/<?php echo $result['profileimage']  ?>" width="250px" height="250px">
                <?php
               if(!$result['profileimage']){
               echo "
                <form action='includes/passenger_image.php' method='POST' enctype = 'multipart/form-data'>
                <div>
                    <input type ='file' name = 'image' required/>           
                </div>
                
                <div class='btn-3'>
                    <input type='submit' id='submit' value='submit'
                     style='background:green; padding:7px 15px; margin: 15px 100px;'>
                </div>
            </form>";
               }
            ?>
            </div>
            <div class="details">
                <?php
               
                echo "
               
                <h3>Name: $result[fname] $result[lname]</h3>
                <h3>Id: $result[id]</h3>
                <h3>Phone no: $result[phoneno]</h3>
                <h3>Email: $result[email]</h3>
                <h3>UserName: $result[username]</h3>
                <h3>Cnic: $result[cnic]</h3>
                
                <a href='#'>Edit Profile</a>
                ";
                ?>
            </div>
        </div>
       
       <?php
       
       include 'pending_request.php';
       
       ?>
        

    </section>

    <!--  -->
</body>

</html>