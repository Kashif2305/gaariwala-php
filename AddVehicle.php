<?php
        session_start();
        if(!isset($_SESSION["Uid"]) || !isset($_SESSION["Uusername"])){
            header("location: login.php?error=notloggedin");
              exit();
        }
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>Add Vehicle</title>
</head>

<body>
    <?php
        
        include 'Owner_Nav.php';
    ?>
    <!-- Main Container-->
 
    <div class="main" style="justify-content:center; text-align:center; height:80%; margin-top:100px;">
        <div class="right Owner-right">
            <div class="logo">
                <!-- GaariWala Logo -->
            </div>
            <h2>Register a Vehicle</h2>
            <form action="includes/AddVehicle.inc.php" method="POST" enctype = "multipart/form-data">
                <div class="Name">
                    <input type="text" name="VehicleName" id="VehicleName" placeholder="Vehicle Name" required>
                    <input type="text" name="VehicleBrand" id="VehicleBrand" placeholder="Vehicle Brand" required>
                </div>
                <div class="Email_Phone">
                    <input type="text" name="RegistrationNum" id="RegistrationNum" placeholder="Registration Number" required>
                    <input type="text" name="TotalSeats" id="TotalSeats" placeholder="TotalSeats" required>
                </div>
                <div class="Username_Pass">
                    <input type="text" name="RouteFrom" id="RouteFrom" placeholder="RouteFrom" required>
                    <input type="text" name="RouteTo" id="RouteTo" placeholder="RouteTo" required>
                </div>
                <div class="Cnic_CarReg">
               
                    <input type = "file" name = "image" required/>
                                  
                </div>
                
                <div class=" btn-3">
                    <input type="submit" id="submit" value="REGISTER">
                </div>
               
            </form>
            <?php
                if(isset($_GET["error"])){

                    if($_GET["error"]=="failedtoAdd"){
               
                         echo "Some Error occured";
                     }
                     else if($_GET["error"]=="EmptyInput"){
               
                       echo "please fill in all inputs";
                     }
                     else if($_GET["error"]=="none"){
               
                       echo "You vehicle is added successfully";
                     }
               }


            ?>

        </div>

    </div>

</body>

</html>