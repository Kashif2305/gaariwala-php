<?php
session_start();
if(!isset($_SESSION["id"]) || !isset($_SESSION["username"])){
  
    header("location: login.php?error=notloggedin");
    exit();
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="Search.css">
    <title>Search </title>
</head>

<body>
    <?php
        
        include 'passenger_nav.php';
    ?>
    <h1 class="text-center" style="margin:10px ;">Search a Vehicle</h1>
    <section class="search_section">
        <div class="search_bar">
        <form action="passenger_search.php" method="POST">
                FROM &nbsp;&nbsp; <input type="search" name="routefrom" placeholder="Current Location">
                &nbsp;&nbsp;&nbsp;
                TO &nbsp;&nbsp; <input type="search" name="routeto" placeholder="Destination">
                &nbsp;&nbsp;&nbsp;
                <button class="btn">Search</button>
            </form>
        </div>
        <h1 class="text-center" style="margin-bottom: 20px;">Results</h1>
        
        <?php
    if($_SERVER["REQUEST_METHOD"] == "POST"){
    $routefrom = $_POST["routefrom"];
    $routeto = $_POST["routeto"];
    require_once "includes/dbh.inc.php";
     $sql = "SELECT * FROM `vehicle` Where `routefrom`= '$routefrom'AND `routeto`='$routeto'; ";  
     $query = mysqli_query($conn, $sql); 
     $row = mysqli_num_rows($query);
     if($row == 0){
        header("location: passenger_search.php?error=nothing");
        exit();
    }
     while($result = mysqli_fetch_assoc($query)){
        
     echo   "
     <form action='includes/passenger_request.inc.php' method='POST'>
        <div class='gallery' style='border: 1px solid #3085b5;
        background-color: #3084b511;'>
        
                <img src='uploads/$result[vehicleimage]' >
            
        <div class='desc' >
        <h3 id='carname'> Id: $result[Owner_id]</h3>
        <h3 id='carname'> Vehicle Name: $result[vehiclename]</h3>
        <h3 id='regno'>Registration no: $result[registrationnum]</h3>
        <h3 id='regno'>Total seats: $result[totalseats]</h1>
        <h3 id='route'>Route: from $result[routefrom] to $result[routeto]</h3>
        <input type='text' name='requestseats' placeholder='enter number of seats' 
        style='padding: 5px 5px' required >
        <input type='hidden' name='id' value='$result[id]' >
        <input type='hidden' name='Owner_id' value='$result[Owner_id]' >
        <input type='hidden' name='name' value='$result[vehiclename]' >
        <input type='hidden' name='num' value='$result[registrationnum]' >
        <input type='hidden' name='from' value='$result[routefrom]' >
        <input type='hidden' name='to' value='$result[routeto]' >
        <input type='hidden' name='seats' value='$result[totalseats]' >
        <button class='btn'>Book</button>
        </form>	
        </div>
        </div>
            ";
        }
    }
    
       
            if(isset($_GET["error"])){

            if($_GET["error"]=="failedtorequest"){

                echo "Can not pick and drop request";
            }
            else if($_GET["error"]=="none"){

            echo "successfully placed pick and drop request";
            }
            else if($_GET["error"]=="problem"){

                echo "There is something wrong";
                }
                else if($_GET["error"]=="nothing"){

                    echo "No record found. Try something else!";
                    }
            }
?>
    </section>
</body>

</html>