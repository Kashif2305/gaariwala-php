<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.3/css/all.css"
        integrity="sha384-SZXxX4whJ79/gErwcOYf+zWLeJdY/qpuqC4cAa9rOGUstPomtqpuNWT9wdPEn2fk" crossorigin="anonymous">
        <link rel="stylesheet" href="Profile.css">
</head>

<body>



<?php
    
     
     $id = $_SESSION["Uid"];
     require_once "includes/dbh.inc.php";
     $sql = "
     SELECT passenger.fname, lname, pick_drop_request.id, vehiclename,registrationnum, routefrom, routeto, vehiclestatus
     FROM passenger INNER JOIN pick_drop_request ON passenger.id = pick_drop_request.passenger_id
     WHERE pick_drop_request.Owner_id= $id;";  
     $query = mysqli_query($conn, $sql); 
     $row = mysqli_num_rows($query);
     if($row == 0){
        echo "No booking data to show";
    }
     while($result = mysqli_fetch_assoc($query)){
    echo "
    <h1 class='text-center'>Passenger pick/drop Requests</h1>
    <div class='Request'>
        <div class='each' >
            <h2 id='carname'>passenger: $result[fname] $result[lname]</h2>
            <h2 id='CarRegno'>Vehicle: $result[vehiclename]</h2>
            <h2 id='CarRegno'>registration no: $result[registrationnum]</h2>
            <div class='address'>
                <h2 id='to' style='color:green'>From: $result[routefrom]</h2>
                <h2 id='from' style='color:red'>To: $result[routeto]</h2>
            </div>";
            
            if($result['vehiclestatus']==0){
                echo "
                <form action='includes/accept_reject.inc.php' method='POST'>
            
                <input type='hidden' name='id' value='$result[id]'>
                
                  <input type='submit' name='Accept' value='Accept' style='background:green;color:black;padding:5px 15px'>
                  <input type='submit' name='Reject' value='Reject' style='background:red;color:black;padding:5px 15px'>
                </div>
                </form>
                ";
            }
           
            else if($result['vehiclestatus']==1){
                
                echo"<h2>Accepted</h2>";
                
            }
            else if($result['vehiclestatus']==2){
                
                echo"<h2>Rejected</h2>";

            }
            ?>
            <?php
           echo "
        </div>
    </div>
    
    ";
     }
    
    ?>    

    </body>

    </html>