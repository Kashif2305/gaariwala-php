<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.3/css/all.css"
        integrity="sha384-SZXxX4whJ79/gErwcOYf+zWLeJdY/qpuqC4cAa9rOGUstPomtqpuNWT9wdPEn2fk" crossorigin="anonymous">
        <link rel="stylesheet" href="Profile.css">
    <title>Login</title>
</head>

<body>


<h1 class="text-center">Your Request for Booking</h1>
     <section>   
    <?php
    
     require_once "includes/dbh.inc.php";
     $id = $_SESSION["id"];
     $sql = "SELECT * FROM `pick_drop_request` WHERE passenger_id = $id; ";  
     $query = mysqli_query($conn, $sql); 
     $row = mysqli_num_rows($query);
     if($row == 0){
        echo "No booking data to show";
    }
     while($result = mysqli_fetch_assoc($query)){
    echo "
        <div class='Request'>
            <div class='each'>
               
                <h2 id='ownername'>$result[vehiclename]</h2>
                <h2 id='CarRegno'>$result[registrationnum]</h2>
                <div class='address'>
                    <h2 id='to' style='color:green'>From: $result[routefrom]</h2>
                    <h2 id='from' style='color:red'>To: $result[routeto]</h2>
                </div>";
                ?>

                <?php
                if($result["vehiclestatus"]==0){
                    echo "
                <div class='status'>
                <h2  style='color:black'> pending</h2>
                </div>";
                
                }
                else if($result["vehiclestatus"]==1){
                    echo "
                <div class='status'>
                <h2  style='color:green'> approved</h2>
                </div>";
                
                }
                else if($result["vehiclestatus"]==2){
                    echo "
                <div class='status'>
                <h2  style='color:red'> rejected</h2>
                </div>";
                
                }
                ?>
                <?php
                echo "
            </div>
        </div>
        ";
     }
    ?>    
    </section>
        </body>
        </html>