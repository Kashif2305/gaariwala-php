<?php
session_start();
if(isset($_SESSION["id"])){
  
    header("location: passenger_search.php");
    exit();
}else if(isset($_SESSION["Uid"])){
    header("location: addvehicle.php");
    exit();
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.3/css/all.css"
        integrity="sha384-SZXxX4whJ79/gErwcOYf+zWLeJdY/qpuqC4cAa9rOGUstPomtqpuNWT9wdPEn2fk" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
    <title>Login</title>
</head>

<body>
    <div class="main">
        <div class="image">
            <img src="images/bg-2.png">
        </div>
        <div class="Content">
            <div class="logo">
                <!-- GaariWala Logo -->
            </div>
            <h2>USER LOGIN</h2>
            <form action="includes/login.inc.php" method="POST">
                <div class="Username">
                    <span><i class="fas fa-user"></i></span>
                    <input type="text" name="uname" id="Username" placeholder="Username">
                </div>
                <div class="password">
                    <span><i class=" fas fa-unlock-alt"></i></span>
                    <input type="password" name="psw" id="Pass" placeholder="Password">
                </div>
                <div class="password">
                <select name="LoginAs" >
                    <option value="VehicleOwner">Login As VehicleOwner</option>
                    <option value="Passenger">Login As Passenger</option>  
                </select>
                </div>
                <div class="btny">
                    <span><i class="fas fa-sign-in-alt"></i></span>
                    <input type="submit" id="submit" value="LOGIN">
                </div>
                <div class="link">
                    <a href="Customer-Reg.php">Forget password?</a>
                </div>
                <br>
                <br>
                <div class="links-2">
                    <p>Doesn't have an account !!<a href="Customer-Reg.php">Sign-up</a></p>
                </div>
            </form>

            <?php
                    if(isset($_GET["error"])){

                    if($_GET["error"]=="failedtologin"){

                        echo "wrong login info";
                    }
                    else if($_GET["error"]=="EmptyInput"){

                        echo "please fill in all inputs";
                    }
                    else if($_GET["error"]=="notloggedin"){

                        echo "Access denied because you are not logged In";
                    }
                    
                    }

            ?>
        </div>
    </div>

</body>

</html>