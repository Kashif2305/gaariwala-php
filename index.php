<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.3/css/all.css"
        integrity="sha384-SZXxX4whJ79/gErwcOYf+zWLeJdY/qpuqC4cAa9rOGUstPomtqpuNWT9wdPEn2fk" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
    <title>Homepage</title>
</head>

<body>
    <!-- main wrapper -->
    <div class="wrapper">
        <!-- navbar part -->
        <div class="navbar">
            <img src="images/logo.png" alt="" style="width: 70px;margin-left: 10px;">
            <div class="nav-items">
                <div class="nav-left">
                    <a href="#about">About</a>
                    <a href="#serv">Services</a>
                    <a href="#team">Team</a>
                    <a href="#">Privacy Policy</a>
                </div>
                <div class="nav-right">
                    <a href="Login.php"><button class="btn-Login">Login</button></a>
                    <a href="#signup"><button class="btn-Signup">Signup</button></a>
                </div>
            </div>
        </div>
        <!-- main-container part -->
        <div class="container">
            <div class="container-content">
                <h1 style="text-transform: uppercase;letter-spacing: 1px;">Travel with confidence</h1>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Alias ea est molestiae necessitatibus quam a
                    iste nulla!.</p>
                <button class="explore-btn">Explore more</button>
            </div>
            <div class="container-image" width="300px" height="300px">
                <img src="images/bg-main.png" alt="">
            </div>
        </div>

    </div>

    <br>
    <!-- About -->
    <section id="about">
        <h2>Making life of people much easier by making<br>travelling easier and cool </h2>
        <br>
        <div class="about">
            <!-- about section left -->
            <div class="about-left">
                <!-- row-1 -->
                <div class="row-1">
                    <div class="img">
                        <img src="images/about-2.jpg" width="100%" height="100%">
                    </div>
                    <div class="text" style="background-color: rgba(228, 218, 174, 0.911);">
                        <h1>Our happy<br>
                            customers</h1>
                        <p>Lorem ipsum dolor sit amet <br>consectetur adipisicing elit.<br>consectetur adipisicing elit.
                        </p>
                    </div>
                </div>
                <!-- row-2 -->
                <div class="row-1">
                    <div class="text" style="background-color: rgba(228, 200, 76, 0.815);">
                        <h1>Ensuring your <br>
                            safety</h1>
                        <p>Lorem ipsum dolor sit amet <br>consectetur adipisicing elit.<br>consectetur adipisicing elit.
                        </p>
                    </div>
                    <div class="img">
                        <img src="images/about-3.png" width="100%" height="100%">
                    </div>
                </div>
            </div>
            <!-- about section left -->
            <div class="about-right">
                <div class="row-1">
                    <span><img src="images/about-1.jpg" alt="" width="60%"></span>
                </div>
                <div class="row-1">
                    <div class="text-1">
                        <h1>Our captains are our families</h1>
                        <p style="color:black;line-height: 25px;">
                            Lorem ipsum dolor sit amet consectetur adipisicing elit.<br>
                            Dolores impedit exercitationem voluptatibus nostrum n<br>
                            Lorem ipsum dolor sit amet consectetur adipisicing elit.<br>
                            Dolores impedit exercitationem voluptatibus nostrum n<br>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <br>
    <br id="serv">

    <!-- Services -->

    <section>
        <h2 style="margin-top:5px ;">One app that has it all</h2>
        <!-- main services -->
        <div class="services">
            <!-- row-1 -->
            <div class="row-1" style="margin-bottom: 40px;">
                <!-- each service -->
                <div class="service">
                    <img src="images/services-2.png" alt="" width="150px">
                    <h3>CAR</h3>
                </div>
                <div class="service">
                    <img src="images/services-1.png" alt="" width="150px">
                    <h3>BIKE</h3>
                </div>
                <div class="service">
                    <img src="images/services-3.png" alt="" width="150px">
                    <h3>VAN</h3>
                </div>
                <div class="service">
                    <img src="images/services-4.png" alt="" width="150px">
                    <h3>COASTER</h3>
                </div>
            </div>
        </div>
        <hr width="25%"
            style="margin-left: 40%; height:1vh; background-color:#e7515b; border: none;margin-bottom: 15px;">
        <div class="Cont-1">
            <div class="image-sec">
                <img src="images/services-5.png">
            </div>
            <div class="content-sec">
                <h1>Making your travel happen<br> on few clicks</h1>
                <p style="color: black;margin-bottom: 10px;">Lorem ipsum dolor sit, amet consectetur adipisicing elit.
                    Obcaecati natus iure
                    eos eaque? Quasi sunt
                    illo omnis nihil exercitationem aspernatur, ullam, iure assumenda, veritatis molestiae corrupti
                    totam tempora quaerat earum.</p>
                <a href="#signup"><button class="btn-Signup"
                        style="letter-spacing: 1px;margin: 10px;">Register</button></a>
            </div>
        </div>
    </section>



    <!-- Team -->

    <div class="team" id="team">
        <h2 style="margin-top:50px;text-align: center;">Faces behind the Gaariwala</h2>
        <p style="color: black;text-align: center;margin-top:15px;width: 50%;margin-left: 25%;">Lorem ipsum, dolor sit
            amet consectetur adipisicin elit. Unde,eaque optio. Nemo, ipsa. Consequatur ratione amet est hic
            voluptatibus, reiciendis dignissimos, distinctio
            nesciunt porro nisi aspernatur ipsum voluptas at! Tenetur?</p>
        <!-- Card No 1 -->
        <div class="wrap" style="padding-top: 20px;">
            <div class="card">
                <div class="card-image">
                    <img src="images/services-2.png" alt="">
                </div>
                <div class="card-content">
                    <h2 style="padding-top: 15px;">Syed Muhammad Kashif</h2>
                    <ul class="list-1">
                        <li>Project Manager</li>
                        <li>Director Gaariwala</li>
                        <li>Web Developer</li>
                    </ul>
                    <button class="btn"
                        style="border:none;background-color: white;color: #e7515b;font-weight: bold; border-radius: 30px;">Follow</button>
                </div>
            </div>
            <!-- Card no 2 -->
            <div class="card">
                <div class="card-image">
                    <img src="images/services-2.png" alt="">
                </div>
                <div class="card-content">
                    <h2 style="padding-top: 15px;">Muhammad Shoaib Tippu</h2>
                    <ul class="list-1">
                        <li>Project Manager</li>
                        <li>Director Gaariwala</li>
                        <li>Web Developer</li>
                    </ul>
                    <button class="btn"
                        style="border:none;background-color: white;color: #e7515b;font-weight: bold; border-radius: 30px;">Follow</button>
                </div>
            </div>

            <!-- Card no 3 -->
            <div class="card">
                <div class="card-image">
                    <img src="images/services-2.png" alt="">
                </div>
                <div class="card-content">
                    <h2 style="padding-top: 15px;">Syed Moin ul Hassan Kazmi</h2>
                    <ul class="list-1">
                        <li>Project Manager</li>
                        <li>Director Gaariwala</li>
                        <li>Web Developer</li>
                    </ul>
                    <button class="btn"
                        style="border:none;background-color: white;color: #e7515b;font-weight: bold; border-radius: 30px;">Follow</button>
                </div>
            </div>
        </div>
    </div>




    <!-- SignUp -->
    <div class="signup-wrapper" id="signup">
        <h2 style="margin-bottom:15px;text-align: center;">Register with us</h2>
        <div class="signup">
            <div class="card-1">
                <h1 style="color: black;letter-spacing: 1px;">Become vehicle Owner</h1>
                <p style="color: black;letter-spacing: 2px;margin-top: 20px;">Start earning today with
                    you
                    <br> Gaariwala Web App
                </p>
                <a href="Owner-Reg.php"><button class="btn-Signup"
                        style="letter-spacing: 1px;margin-top: 20px;font-size: 18px;">Signup</button></a>
            </div>
            <div class="card-2">
                <h1 style="color: white;letter-spacing: 1px;">Become a passenger</h1>
                <p style="color:white;letter-spacing: 2px;margin-top: 20px;">Start earning today with you
                    <br> Gaariwala Wpeb App
                </p>
                <a href="passenger_reg.php"><button class="btn-Signup"
                        style="color: white;letter-spacing: 1px;margin-top: 20px;font-size: 18px;">Signup</button>
                </a>
            </div>
        </div>
        <div class="becomes-bg" style="transform: translate(0px, 0px); opacity:1;">

        </div>
    </div>

    <!-- footer -->

    <div class="row">
        <!-- part-1 -->
        <div class="logo">
            <img src="images/logo.png" alt="" width="150px">
        </div>
        <!-- part-2 -->
        <div class="nav-left links">
            <ul>
                <li><a href="#about" style="font-size: 20px;margin-top: 50%;font-weight: bold;">About</a></li>
                <li><a href="#serv" style="font-size: 20px;margin-top: 50%;font-weight: bold;">Services</a></li>
                <li><a href="#team" style="font-size: 20px;margin-top: 50%;font-weight: bold;">Team</a></li>
                <li><a href="#" style="font-size: 20px;margin-top: 50%;font-weight: bold;">Privacy Policy</a></li>
            </ul>
        </div>
        <!-- part-3 -->
        <div class="icons">
            <ul>
                <li><a href="#about"><i style="color: #3085b5;" class="fab fa-facebook-square fa-3x"></i></a></li>
                <li><a href="#serv"><i style="color: #3085b5;" class="fab fa-twitter-square fa-3x"></i></a></li>
                <li><a href="#team"><i style="color: #e7515b" class="fab fa-instagram-square fa-3x"></i></a></li>
            </ul>
        </div>

    </div>
    <hr width="50%" style="margin-left: 25%; height:0.3vh; background-color:#a53f3f; border: none;margin-bottom: 15px;">

    <!-- footer -->
    <footer>
        <div class="foot">
            <p style="padding: 15px;background-color: #3085b5;">© GaariWala.com, Inc. All rights
                reserved</p>
        </div>
    </footer>


</body>

</html>