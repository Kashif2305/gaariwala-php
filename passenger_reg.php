<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>Customer Registration</title>
</head>

<body>
    <!-- Main Container-->
    <div class="main">
        <!-- Image Section -->
        <div class="left">
            <h2> A few clicks away from booking<br>
                your Ride!!!
            </h2>
            <img src="images/bg-3.png" alt="">
        </div>
        <!-- Form Section -->
        <div class="right">
            <div class="logo">
                <!-- GaariWala Logo -->
            </div>
            <h2>Register as Customer</h2>
            <form action="includes/passenger_reg.inc.php" method="POST">
                <div class="Name">
                    <input type="text" name="firstName"  placeholder="FirstName">
                    <input type="text" name="LastName"  placeholder="LastName">
                </div>
                <div class="Email_Phone">
                    <input type="email" name="Email" placeholder="Email">
                    <input type="tel" name="Phone"  placeholder="PhoneNo">
                </div>
                <div class="Username_Pass">
                    <input type="text" name="UserName"  placeholder="Username">
                    <input type="password" name="Password"  placeholder="Password">
                </div>
                <div class="Adress_Cnic">
                    
                <input type="password" name="Pass-repeat"  placeholder="RepeatPassword">
                    <input type="text" name="Cnic"  placeholder="Cnic">
                </div>
                <div class="btn-2">
                    <input type="submit" id="submit" value="REGISTER">
                </div>
                <a href="index.php">home</a>
            </form>
            <?php
                if(isset($_GET["error"])){

                    if($_GET["error"]=="failedtosignup"){
               
                         echo "We failed to register you on our system";
                     }
                     else if($_GET["error"]=="EmptyInput"){
               
                       echo "please fill in all inputs";
                     }
                     else if($_GET["error"]=="InvalidUid"){
               
                         echo "invalid username";
                     }
                     else if($_GET["error"]=="InvalidEmail"){
               
                       echo "Invalid Email";
                     }
                     else if($_GET["error"]=="passnotmatch"){
               
                       echo "Passwords don't match";
                     }
                     else if($_GET["error"]=="Uidexists"){
               
                       echo "The username or email already exists in our system";
                     }
                     else if($_GET["error"]=="none"){
               
                             echo "You are registered on our system sucessfully";
                     }
               }


            ?>
        </div>
    </div>




</body>

</html>