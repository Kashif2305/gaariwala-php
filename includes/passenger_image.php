<?php 
session_start();
if(isset($_FILES['image'])){


    $errors= array();
    $file_name = $_FILES['image']['name'];
    $file_size = $_FILES['image']['size'];
    $file_tmp = $_FILES['image']['tmp_name'];
    $file_type = $_FILES['image']['type'];


    require_once "dbh.inc.php";


  
    $FileExt = explode('.',$file_name);
    $FileActualExt = strtolower(end( $FileExt));
    
    $extensions= array("jpeg","jpg","png");
    
    if(in_array($FileActualExt,$extensions)=== false){
       $errors[]="wrong file type. this extension is not allowed";
    }
    
    if($file_size > 9097152) {
       $errors[]='file size is too big';
    }
    
    if(empty($errors)==true) {
           
      $FileNewName = "passengers".rand(1000, 9999).".".$FileActualExt;
      
       move_uploaded_file($file_tmp,'E:\web development\htdocs\website Project\passengers/'.$FileNewName);
       
       $id = $_SESSION["id"];
       $sql = "UPDATE `passenger` SET `profileimage`='$FileNewName' 
       WHERE `id`=$id;";  
       
   
       if($query = mysqli_query($conn, $sql)){

            mysqli_close($conn);
            header("location: ../passenger_profile.php?error=none");
            exit();
    
       }
       else{

            header("location: ../passenger_profile?error=failedtoAdd");
            exit();
       }
 
    }else{
       print_r($errors);
    }
}
else{
 
    header("location: ../passenger_profile.php?error=failedtoAdd");
    exit();
}
