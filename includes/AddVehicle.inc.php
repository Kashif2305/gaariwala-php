<?php 
session_start();
if($_SERVER["REQUEST_METHOD"] == "POST" && isset($_FILES['image'])){
$vehiclename=$_POST["VehicleName"];
$vehiclebrand=$_POST["VehicleBrand"];
$reg_num = $_POST["RegistrationNum"];
$totalseats = $_POST["TotalSeats"];
$routefrom=$_POST["RouteFrom"];
$routeto = $_POST["RouteTo"];

$errors= array();
    $file_name = $_FILES['image']['name'];
    $file_size = $_FILES['image']['size'];
    $file_tmp = $_FILES['image']['tmp_name'];
    $file_type = $_FILES['image']['type'];


require_once "dbh.inc.php";
require_once "../functions.php";

    
  
    $owner_id = $_SESSION["Uid"];
    $FileExt = explode('.',$file_name);
    $FileActualExt = strtolower(end( $FileExt));
    
    $extensions= array("jpeg","jpg","png");
    
    if(in_array($FileActualExt,$extensions)=== false){
       $errors[]="wrong file type. this extension is not allowed";
    }
    
    if($file_size > 9097152) {
       $errors[]='file size is too big';
    }
    
    if(empty($errors)==true) {
           
      $FileNewName = "vehicle".rand(1000, 9999).".".$FileActualExt;
      
       move_uploaded_file($file_tmp,'E:\web development\htdocs\website Project\uploads/'.$FileNewName);
      
       mysqli_stmt_close($stmt);
    }else{
       print_r($errors);
    }
    AddVehicle($conn,$vehiclename,$vehiclebrand,$reg_num,$totalseats,$routefrom,$routeto,$FileNewName);
}
else{
    header("location: ../AddVehicle.php?error=failedtoAdd");
    exit();
}

