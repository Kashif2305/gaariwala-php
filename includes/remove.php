<?php
require_once "dbh.inc.php";
$id = $_POST['id'];
$sql = "DELETE FROM vehicle WHERE id=$id";

if (mysqli_query($conn, $sql)) {
    header("location: ../view_vehicle.php?error=deleted");
    exit();
} else {
    header("location: ../view_vehicle.php?error=notdeleted");
    exit();
}

mysqli_close($conn);

?>
