<?php
session_start();

require_once "dbh.inc.php";
if($_SERVER["REQUEST_METHOD"] == "POST"){

    $vehicle_id = $_POST["id"];
    $Owner_id = $_POST["Owner_id"];
    $passenger_id = $_SESSION["id"];
    $vehiclename=$_POST["name"];
    $reg_num = $_POST["num"];
    $totalseats = $_POST["seats"];
    $routefrom=$_POST["from"];
    $routeto = $_POST["to"];
    $requested= $_POST["requestseats"];
    $status = 0;

/*
    $sql = "SELECT * FROM `vehicle` Where `id`= $vehicle_id; ";  
    $query = mysqli_query($conn, $sql); 
    $row = mysqli_num_rows($query);
    if($row == 0){
       echo "No results found";
   }
   $result = mysqli_fetch_assoc($query)
   */

   $qry = "INSERT INTO pick_drop_request (passenger_id,vehicle_id,Owner_id,requestseats,vehiclename,registrationnum,totalseats,routefrom,routeto,vehiclestatus)
    VALUES(?,?,?,?,?,?,?,?,?,?);";
   $stmt= mysqli_stmt_init($conn);

   if(!mysqli_stmt_prepare($stmt, $qry)){

       header("location: ../passenger_search.php?error=failedtorequest");
       exit();
   }
   

   mysqli_stmt_bind_param($stmt, 'ssssssssss',$passenger_id,$vehicle_id,$Owner_id,$requested,$vehiclename,$reg_num,$totalseats,$routefrom,$routeto,$status  );
   mysqli_stmt_execute($stmt);
   mysqli_stmt_close($stmt);

   header("location: ../passenger_search.php?error=none");
   exit();

  echo mysqli_error($conn);
    
}
else{
    
    header("location: ../passenger_search.php?error=problem");
    exit();
}