<?php 

if($_SERVER["REQUEST_METHOD"] == "POST"){
$fname=$_POST["firstName"];
$lname=$_POST["LastName"];
$email=$_POST["Email"];
$phoneno = $_POST["Phone"];
$username=$_POST["UserName"];
$psw = $_POST["Password"];
$psw_repeat = $_POST["Pass-repeat"];
$cnic = $_POST["Cnic"];

require_once "dbh.inc.php";
require_once "../functions.php";

if(EmptyInputSignup($fname,$lname,$email,$phoneno,$username,$psw,$psw_repeat,$cnic)!==false){

    header("location: ../passenger_reg.php?error=EmptyInput");
    exit();

}

if(InvalidUid($username)!==false){
    header("location: ../passenger_reg.php?error=InvalidUid");
    exit();
}

if(InvalidEmail($email)!==false){
    header("location: ../passenger_reg.php?error=InvalidEmail");
    exit();
}

if(pass_match($psw,$psw_repeat)!==false){
    header("location: ../passenger_reg.php?error=passnotmatch");
    exit();
}

if(Uidexists($conn,$username,$email)!==false){
    header("location: ../passenger_reg.php?error=Uidexists");
    exit();
}


createpassenger($conn,$fname,$lname,$email,$phoneno,$username,$psw,$psw_repeat,$cnic);


}
else{
    header("location: ../Owner-Reg.php?error=failedtosignup");
}

