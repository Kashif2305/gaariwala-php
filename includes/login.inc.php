<?php 

if($_SERVER["REQUEST_METHOD"] == "POST"){

$username=$_POST["uname"];
$psw = $_POST["psw"];
$selected_val = $_POST['LoginAs'];


require_once "dbh.inc.php";
require_once "../functions.php";

if(EmptyInputLogin($username,$psw,$selected_val)!==false){

    header("location: ../login.php?error=EmptyInput");
    exit();

}

if($selected_val == "VehicleOwner"){
 
    loginOwner($conn,$username,$psw);
}
else if($selected_val == "Passenger"){
   
    loginPassenger($conn,$username,$psw);
}

}
else{
    header("location: ../login.php?error=failedtologin");
}
