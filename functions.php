<?php
function EmptyInputSignup($fname,$lname,$email,$phoneno,$username,$psw,$psw_repeat,$cnic){

    $result;
    if(empty($fname) || empty($lname) || empty($email) || empty($phoneno) ||
    empty($username) || empty($psw) || empty($psw_repeat) || empty($cnic) ){
        $result=true;
    }else {
        $result=false;
    }

    return $result;
}

function InvalidUid($username){
    $result;
    if(!preg_match("/^[a-zA-Z0-9]*$/",$username)){
        $result=true;
    }else {
        $result=false;
    }

    return $result;
}

function InvalidEmail($email){
    $result;
    if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
        $result=true;
    }else {
        $result=false;
    }

    return $result;

}

function pass_match($psw,$psw_repeat){
    $result;
    if($psw !== $psw_repeat){
        $result=true;
    }else {
        $result=false;
    }

    return $result;
}

function Uidexists($conn,$username,$email){
    $result;
    $sql = "SELECT * FROM vehicleowner WHERE username=? OR email=?;";
    $stmt= mysqli_stmt_init($conn);

    if(!mysqli_stmt_prepare($stmt, $sql)){

        header("location: ../Owner-Reg.php?error=stmtfailed");
        exit();
    }
   
    mysqli_stmt_bind_param($stmt, 'ss', $username,$email );
    mysqli_stmt_execute($stmt);
    $result = mysqli_stmt_get_result($stmt);
    if($row = mysqli_fetch_assoc($result)){

        return $row;
    }else {
        $result=false;
        return $result;
    }
    mysqli_stmt_close($stmt);
}

function UidexistsPassenger($conn,$username,$email){
    $result;
    $sql = "SELECT * FROM passenger WHERE username=? OR email=?;";
    $stmt= mysqli_stmt_init($conn);

    if(!mysqli_stmt_prepare($stmt, $sql)){

        header("location: ../passenger_reg.php?error=stmtfailed");
        exit();
    }
   
    mysqli_stmt_bind_param($stmt, 'ss', $username,$email );
    mysqli_stmt_execute($stmt);
    $result = mysqli_stmt_get_result($stmt);
    if($row = mysqli_fetch_assoc($result)){

        return $row;
    }else {
        $result=false;
        return $result;
    }
    mysqli_stmt_close($stmt);
}

function createOwner($conn,$fname,$lname,$email,$phoneno,$username,$psw,$psw_repeat,$cnic){

    $sql = "INSERT INTO vehicleowner (fname, lname, email, phoneno, username, password, cnic) VALUES(?,?,?,?,?,?,?);";
    $stmt= mysqli_stmt_init($conn);

    if(!mysqli_stmt_prepare($stmt, $sql)){

        header("location: ../Owner-Reg.php?error=failedtosignup");
        exit();
    }
    $hashedPass= password_hash($psw, PASSWORD_DEFAULT);

    mysqli_stmt_bind_param($stmt, 'sssssss', $fname,$lname,$email,$phoneno,$username,$hashedPass,$cnic );
    mysqli_stmt_execute($stmt);
    mysqli_stmt_close($stmt);
    header("location: ../Owner-Reg.php?error=none");
    exit();


}

function createpassenger($conn,$fname,$lname,$email,$phoneno,$username,$psw,$psw_repeat,$cnic){

    $sql = "INSERT INTO passenger (fname, lname, email, phoneno, username, password, cnic) VALUES(?,?,?,?,?,?,?);";
    $stmt= mysqli_stmt_init($conn);

    if(!mysqli_stmt_prepare($stmt, $sql)){

        header("location: ../passenger_reg.php?error=failedtosignup");
        exit();
    }
    $hashedPass= password_hash($psw, PASSWORD_DEFAULT);

    mysqli_stmt_bind_param($stmt, 'sssssss', $fname,$lname,$email,$phoneno,$username,$hashedPass,$cnic );
    mysqli_stmt_execute($stmt);
    mysqli_stmt_close($stmt);
    header("location: ../passenger_reg.php?error=none");
    exit();


}

//----------Now login Functions are implemented here----------------

function EmptyInputLogin($username,$psw,$selected_val){

    $result;
    if(empty($username) || empty($psw) || empty($selected_val)){
        $result=true;
    }else {
        $result=false;
    }

    return $result;
}

function loginOwner($conn,$username,$psw){

    $UidExists = Uidexists($conn,$username,$username);

    
    if($UidExists === false){
        header("location: ../login.php?error=failedtologin");
        exit();
    }

    $psw_hashed = $UidExists["password"];
    $chcked_psw = password_verify($psw, $psw_hashed);
   
    if ($chcked_psw===false) {
       
        header("location: ../login.php?error=failedtologin");
        exit();
    }
    else if($chcked_psw===true) {
        session_start();
        $_SESSION["Uid"] = $UidExists["id"];
        $_SESSION["Uusername"]=$UidExists["username"];

        header("location: ../AddVehicle.php");
        exit();
    }

}

function loginPassenger($conn,$username,$psw){

    $UidExists = UidexistsPassenger($conn,$username,$username);

    
    if($UidExists === false){
        header("location: ../login.php?error=failedtologin");
        exit();
    }

    $psw_hashed = $UidExists["password"];
    $chcked_psw = password_verify($psw, $psw_hashed);
   
    if ($chcked_psw===false) {
       
        header("location: ../login.php?error=failedtologin");
        exit();
    }
    else if($chcked_psw===true) {
        session_start();
        $_SESSION["id"] = $UidExists["id"];
        $_SESSION["username"]=$UidExists["username"];

        header("location: ../passenger_search.php");
        exit();
    }

}


//------------------functions to Add vehicle to our system------------------------

function VehicleRegExist($conn,$reg_num){
    $result;
    $sql = "SELECT * FROM vehicle WHERE registrationnum=?;";
    $stmt= mysqli_stmt_init($conn);

    if(!mysqli_stmt_prepare($stmt, $sql)){

        header("location: ../AddVehicle.php?error=stmtfailed");
        exit();
    }
   
    mysqli_stmt_bind_param($stmt, 's', $reg_num );
    mysqli_stmt_execute($stmt);
    $result = mysqli_stmt_get_result($stmt);
    if($row = mysqli_fetch_assoc($result)){

        return $row;
    }else {
        $result=false;
        return $result;
    }
    mysqli_stmt_close($stmt);
}





function AddVehicle($conn,$vehiclename,$vehiclebrand,$reg_num,$totalseats,$routefrom,$routeto,$file_name){

   $Owner_id = $_SESSION["Uid"];
    $sql = "INSERT INTO vehicle (Owner_id,vehiclename,vehiclebrand,registrationnum,totalseats,routefrom,routeto,vehicleimage) 
    VALUES(?,?,?,?,?,?,?,?);";
    $stmt= mysqli_stmt_init($conn);

    if(!mysqli_stmt_prepare($stmt, $sql)){

        header("location: ../login.php?error=failedtoAdd");
        exit();
       
    }
    
    mysqli_stmt_bind_param($stmt, 'ssssssss', $Owner_id,$vehiclename,$vehiclebrand,$reg_num,$totalseats,$routefrom,$routeto,$file_name );
    mysqli_stmt_execute($stmt);
    mysqli_stmt_close($stmt);
    header("location: ../AddVehicle.php?error=none");
    exit();


}







