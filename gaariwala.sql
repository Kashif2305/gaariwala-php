-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 30, 2021 at 03:57 PM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 7.3.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gaariwala`
--

-- --------------------------------------------------------

--
-- Table structure for table `passenger`
--

CREATE TABLE `passenger` (
  `id` int(200) NOT NULL,
  `fname` varchar(200) NOT NULL,
  `lname` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `phoneno` int(14) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(500) NOT NULL,
  `cnic` int(14) NOT NULL,
  `profileimage` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `passenger`
--

INSERT INTO `passenger` (`id`, `fname`, `lname`, `email`, `phoneno`, `username`, `password`, `cnic`, `profileimage`) VALUES
(1, 'imran', 'shah', 'imran@gmail.com', 2147483647, 'imran12', '$2y$10$9TMED7Ep.U16.cXaNSfuJ.T1DFx8s7qFU6X4BNEYggimJcW0vrG5S', 2147483647, 'passengers9468.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `pick_drop_request`
--

CREATE TABLE `pick_drop_request` (
  `id` int(20) NOT NULL,
  `passenger_id` int(20) NOT NULL,
  `vehicle_id` int(20) NOT NULL,
  `Owner_id` int(20) NOT NULL,
  `requestseats` int(20) NOT NULL,
  `vehiclename` varchar(200) NOT NULL,
  `registrationnum` varchar(200) NOT NULL,
  `totalseats` int(20) NOT NULL,
  `routefrom` varchar(200) NOT NULL,
  `routeto` varchar(200) NOT NULL,
  `vehiclestatus` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pick_drop_request`
--

INSERT INTO `pick_drop_request` (`id`, `passenger_id`, `vehicle_id`, `Owner_id`, `requestseats`, `vehiclename`, `registrationnum`, `totalseats`, `routefrom`, `routeto`, `vehiclestatus`) VALUES
(16, 1, 18, 4, 4, 'gli', 'isl 204', 5, 'islamabad', 'rawalpindi', 2),
(17, 1, 20, 5, 2, 'passo ', 'isl 515', 5, 'islamabad', 'rawalpindi', 1);

-- --------------------------------------------------------

--
-- Table structure for table `vehicle`
--

CREATE TABLE `vehicle` (
  `id` int(20) NOT NULL,
  `Owner_id` int(20) NOT NULL,
  `vehiclename` varchar(50) NOT NULL,
  `vehiclebrand` varchar(50) NOT NULL,
  `registrationnum` varchar(50) NOT NULL,
  `totalseats` int(20) NOT NULL,
  `routefrom` varchar(50) NOT NULL,
  `routeto` varchar(50) NOT NULL,
  `vehicleimage` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `vehicle`
--

INSERT INTO `vehicle` (`id`, `Owner_id`, `vehiclename`, `vehiclebrand`, `registrationnum`, `totalseats`, `routefrom`, `routeto`, `vehicleimage`) VALUES
(17, 4, 'Passo', 'Daihatsu ', 'mul 344', 4, 'islamabad', 'rawalpindi', 'vehicle4909.jpg'),
(18, 4, 'gli', 'toyota', 'isl 204', 5, 'islamabad', 'rawalpindi', 'vehicle8382.jpg'),
(20, 5, 'passo ', 'toyota', 'isl 515', 5, 'islamabad', 'rawalpindi', 'vehicle9699.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `vehicleowner`
--

CREATE TABLE `vehicleowner` (
  `id` int(20) NOT NULL,
  `fname` varchar(30) NOT NULL,
  `lname` varchar(30) NOT NULL,
  `email` varchar(30) NOT NULL,
  `phoneno` int(14) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(200) NOT NULL,
  `cnic` int(13) NOT NULL,
  `profileimage` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `vehicleowner`
--

INSERT INTO `vehicleowner` (`id`, `fname`, `lname`, `email`, `phoneno`, `username`, `password`, `cnic`, `profileimage`) VALUES
(1, 'khan', 'sahab', 'khangi@gmail.com', 2147483647, 'khangi4', '$2y$10$mKj6YmJju/U3K5FTyDCRuOjYbty.mhmwv2i7HcPAdNs3KGoriVSZG', 2147483647, ''),
(2, 'khan', 'sahab', 'khang22i@gmail.com', 2147483647, 'khangi445', '$2y$10$K/dtnM.SrPBUkeKDyCFSe..4J9s6kPEwS3dPwymNp1qNOm2lPL1M.', 2147483647, ''),
(3, 'kashif', 'SYED', 'tatheerabidi00@gmail.com', 2147483647, 'khansahab12', '$2y$10$cyJP41SWrFBP4T5Wetk0QO1ARQZHFDII7NFSaDqezbS.X5H74J2EK', 2147483647, ''),
(4, 'kashif', 'ali', 'syedraz1212@gmail.com', 2147483647, 'syedraz', '$2y$10$CA.YaACYmknJiIuN6H07UO7K/ydMWDeIC2igMSQypaQj4o0lMgjWS', 2147483647, 'Owner6370.jpg'),
(5, 'kashif', 'khan', 'kashif@gmail.com', 2147483647, 'kashif', '$2y$10$GohH4jrtgspTZYYlS64ytuiylCgr4uML0nKq78Do0PUm1JE0WrIQa', 122243445, 'Owner1473.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `passenger`
--
ALTER TABLE `passenger`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pick_drop_request`
--
ALTER TABLE `pick_drop_request`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vehicle`
--
ALTER TABLE `vehicle`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vehicleowner`
--
ALTER TABLE `vehicleowner`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `passenger`
--
ALTER TABLE `passenger`
  MODIFY `id` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `pick_drop_request`
--
ALTER TABLE `pick_drop_request`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `vehicle`
--
ALTER TABLE `vehicle`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `vehicleowner`
--
ALTER TABLE `vehicleowner`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
