<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>Owner Registration</title>
</head>

<body>
    <!-- Main Container-->
    <div class="main">
        <div class="right Owner-right">
            <div class="logo">
                <!-- GaariWala Logo -->
            </div>
            <h2>Register as Vehicle Owner</h2>
            <form action="includes/owner_reg.inc.php" method="POST">
                <div class="Name">
                    <input type="text" name="FirstName" id="firstName" placeholder="FirstName">
                    <input type="text" name="LastName" id="lastName" placeholder="LastName">
                </div>
                <div class="Email_Phone">
                    <input type="email" name="Email" id="email" placeholder="Email">
                    <input type="tel" name="Phone" id="phoneNo" placeholder="PhoneNo">
                </div>
                <div class="Username_Pass">
                    <input type="text" name="UserName" id="Username" placeholder="Username">
                    <input type="password" name="Password" id="pass" placeholder="Password">
                </div>
                <div class="Cnic_CarReg">
                <input type="password" name="Pass-repeat" id="pass" placeholder="Repeat Password">
                    <input type="text" name="Cnic"  placeholder="Cnic">
                    
                </div>
                
                <div class=" btn-3">
                    <input type="submit" id="submit" value="REGISTER">
                </div>
                <a href="index.php">home</a>
            </form>
            <?php
                if(isset($_GET["error"])){

                    if($_GET["error"]=="failedtosignup"){
               
                         echo "We failed to register you on our system";
                     }
                     else if($_GET["error"]=="EmptyInput"){
               
                       echo "please fill in all inputs";
                     }
                     else if($_GET["error"]=="InvalidUid"){
               
                         echo "invalid username";
                     }
                     else if($_GET["error"]=="InvalidEmail"){
               
                       echo "Invalid Email";
                     }
                     else if($_GET["error"]=="passnotmatch"){
               
                       echo "Passwords don't match";
                     }
                     else if($_GET["error"]=="Uidexists"){
               
                       echo "The username or email already exists in our system";
                     }
                     else if($_GET["error"]=="none"){
               
                             echo "You are registered on our system sucessfully";
                     }
               }


            ?>

        </div>
        <!-- Image Section -->
        <div class="Left-owner left">
            <h2 style="letter-spacing: 1px;"> We consider you as our Partner!!!
            </h2>
            <img src="images/bg-4.png" alt="">
        </div>
        <!-- Form Section -->

    </div>





</body>

</html>