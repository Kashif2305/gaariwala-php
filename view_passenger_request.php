<?php
        session_start();
        if(!isset($_SESSION["Uid"]) || !isset($_SESSION["Uusername"])){
            header("location: login.php?error=notloggedin");
              exit();
        }
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.3/css/all.css"
        integrity="sha384-SZXxX4whJ79/gErwcOYf+zWLeJdY/qpuqC4cAa9rOGUstPomtqpuNWT9wdPEn2fk" crossorigin="anonymous">
        <link rel="stylesheet" href="Profile.css">
    <title>View Vehicles</title>
</head>

<body>
        <?php
       
        include 'Owner_Nav.php';
        echo "<br>";
        echo "<br>";
        echo "<br>";
        include 'requested_vehicle.php';


        ?>
</body>
 </html>